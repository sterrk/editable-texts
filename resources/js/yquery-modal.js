/*
 * yQuery-modal 0.0.1
 */
yQuery(function(){
  var modalBg = (function() {
    var ret = document.createElement("div");
    ret.classList.add('yquery-modal-bg','close-yquery-modal');
    ret.style.display = 'none';
    ret.id = 'yquery-modal-bg';
    yQuery.one('body').appendChild(ret);
    return ret;
  })();

  // Extend yQuery object
  yQuery.animations = {
    fadeOut: function(element){
      element.style.opacity = 1;

      (function fade() {
        if ((element.style.opacity -= .1) < 0) {
          element.style.display = "none";
        } else {
          requestAnimationFrame(fade);
        }
      })();
    },
    fadeIn: function(element, display){
      element.style.opacity = 0;
      element.style.display = display || "block";

      (function fade() {
        var val = parseFloat(element.style.opacity);
        if (!((val += .1) > 1)) {
          element.style.opacity = val;
          requestAnimationFrame(fade);
        }
      })();
    }
  };

  yQuery[ 'modal' ] = function(selector, action, origin) {
    var modal = yQuery.one(selector);
    modal.style.top = '100px';
    modal.style.visibility = 'visible';

    if(action === "open") {
      modal.classList.add('open');
      modalBg.setAttribute('data-modal',selector);
      yQuery.animations.fadeIn(modalBg);
      yQuery.animations.fadeIn(modal);
      modal.dispatchEvent(new CustomEvent(
        'yquery.modal.open',
        { detail: { caller: origin } }
      ));
    } else if (action === "close") {
      modal.classList.remove('open');
      yQuery.animations.fadeOut(modalBg);
      yQuery.animations.fadeOut(modal);
      modal.dispatchEvent(new CustomEvent(
        'yquery.modal.close',
        { detail: { caller: origin } }
      ));
    }
  }

  // Attach modals based on .{open|close}-y-modal classes
  yQuery.on('click', yQuery('.open-yquery-modal'), function(event){
    event.preventDefault();
    var modalSelector = this.getAttribute('data-modal');
    if(!modalSelector){
      console.error('yQuery-modal: Data-modal property not defined.');
      return;
    }
    modalBg.setAttribute('data-modal',modalSelector);
    yQuery.modal(modalSelector, 'open', this);
  });

  yQuery.on('click', yQuery('.close-yquery-modal'), function(event){
    event.preventDefault();
    var modalSelector = this.getAttribute('data-modal');
    if(!modalSelector){
      console.error('yQuery-modal: Data-modal property not defined.');
      return;
    }
    yQuery.modal(modalSelector, 'close', this);
  });
});
