<?php

return [
    /**
     * Location where to store uploaded files
     * e.g. `local`, `s3`, `rackspace`
     */
    'image_upload_location' => config('filesystems.default'),

    /**
     * Set the package debug mode
     */
    'debug' => env('APP_DEBUG', false),
];
