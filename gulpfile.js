var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

gulp.task('default', function() {
    gulp.src([
            './resources/js/yquery.js',
            './resources/js/yquery-modal.js',
            './resources/js/et.js',
        ])
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('./resources/js/'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./public/js/'));
    gulp.src([
            './resources/sass/package.scss',
        ])
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./public/css/'));
});
