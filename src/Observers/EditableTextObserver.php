<?php

namespace Sterrk\EditableText\Observers;

use Sterrk\EditableText\Models\EditableText;

class EditableTextObserver
{
    public function saved(EditableText $editableText)
    {
        $editableText->cache();
    }
}
