<?php

namespace Sterrk\EditableText\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class EditableText extends Model
{
    protected $fillable = [
        'category',
        'key',
        'value'
    ];

    /**
     * @param bool $useKeyAsFallback
     * @return string
     */
    public function getValue($useKeyAsFallback = true): string
    {
        if (is_null($this->value) && $useKeyAsFallback) {
            return $this->key;
        }

        return $this->value;
    }

    /**
     * Store model in cache
     */
    public function cache(): void
    {
        Cache::forever(self::generateCacheKey($this->category, $this->key), $this);
    }

    /**
     * Fetch ET model from cache if it exists in cache
     *
     * @param string $category
     * @param string $key
     * @return mixed
     */
    public static function getCached($category, $key)
    {
        return Cache::get(self::generateCacheKey($category, $key));
    }

    /**
     * Generate cache key
     *
     * @param $category
     * @param $key
     * @return string
     */
    public static function generateCacheKey($category, $key): string
    {
        return "$category.$key";
    }
}
