<?php

namespace Sterrk\EditableText\Utilities;

use Sterrk\EditableText\Events\EditModeEnabled;
use Sterrk\EditableText\Events\EditModeDisabled;
use Sterrk\EditableText\Exceptions\EditableContentException;
use Sterrk\EditableText\Models;
use Illuminate\Support\Facades\Cache;

class EditableText
{
    const SESSION_KEY = 'et_edit_mode';

    /**
     * Get text from DB
     *
     * @param string $category
     * @param string $key
     * @param string|null $defaultValue
     * @return string
     * @throws EditableContentException
     */
    public static function getText($category, $key, $defaultValue = null): string
    {
        if (empty($category) || empty($key)) {
            throw new EditableContentException('Category or key missing');
        }

        self::validateString($category);
        self::validateString($key);

        $text = self::getValue($category, $key, $defaultValue);

        return $text;
    }

    /**
     * Validate if string matches rules
     *
     * @param string $string
     * @return bool
     * @throws EditableContentException
     */
    protected static function validateString($string): bool
    {
        if (!preg_match('/^[\w-]+$/', $string)) {
            throw new EditableContentException('Database string can only contain alphanumeric characters, underscores, and dashes: ' . $string);
        }
        return true;
    }

    /**
     * Get the value of ET tag
     *
     * @param string $category
     * @param string $key
     * @param string|null $defaultValue
     * @return string
     */
    protected static function getValue($category, $key, $defaultValue): string
    {
        // Return defaultValue if in debug mode
        if (config('editabletext.debug') === true) {
            return $defaultValue;
        }

        // Try to fetch model from cache
        $editableTextModel = Models\EditableText::getCached($category, $key);

        if (!$editableTextModel instanceof Models\EditableText) {
            $editableTextModel = self::findOrInsert($category, $key, $defaultValue);
            $editableTextModel->cache();
        }

        // Get value, using key as fallback
        $value = $editableTextModel->getValue(true);

        return $value;
    }

    /**
     * FindOrInsert ET model and return it
     *
     * @param string $category
     * @param string $key
     * @param string|null $value
     * @return Models\EditableText
     */
    protected static function findOrInsert($category, $key, $value)
    {
        return Models\EditableText::firstOrCreate(
            compact('category', 'key'),
            compact('value')
        );
    }

    /**
     * @return bool
     */
    public static function isEditModeEnabled(): bool
    {
        return session(self::SESSION_KEY) === true;
    }

    /**
     * @param bool $enabled
     */
    public static function setEditMode($enabled = true)
    {
        session([self::SESSION_KEY => $enabled]);

        if ($enabled) {
            event(new EditModeEnabled());
        } else {
            event(new EditModeDisabled());
        }
    }

    /**
     * @param string $category
     * @param string $key
     * @param string|null $defaultValue
     * @param bool $excludedFromEditMode
     * @return string
     */
    public static function renderText($category, $key, $defaultValue = null, $excludedFromEditMode = false, $isImage = false)
    {
        $text = self::getText($category, $key, $defaultValue);
        if (self::isEditModeEnabled() && !$excludedFromEditMode) {
            return view('et::edit_mode_element', compact('category', 'key', 'text'));
        }
        return $text;
    }

    /**
     * @param string $category
     * @param string $key
     * @param string|null $defaultValue
     * @return string
     * @throws EditableContentException
     */
    public static function renderImage(string $category, string $key, string $defaultValue = null)
    {
        $text = self::getText($category, $key, $defaultValue);
        $text = !empty($text) ? $text : '#';
        if (self::isEditModeEnabled()) {
            return sprintf("src=%s editable-image data-category=%s data-key=%s", $text, $category, $key);
        }
        return 'src=' . $text;
    }
}
