<?php

namespace Sterrk\EditableText\Events;

class EditableTextPublished
{
    /**
     * @var int
     */
    protected $count;

    /**
     * EditableTextPublished constructor.
     * @param int $count
     */
    public function __construct($count)
    {
        $this->count = $count;
    }
}
