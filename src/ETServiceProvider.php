<?php

namespace Sterrk\EditableText;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Sterrk\EditableText\Models\EditableText as ETModel;
use Sterrk\EditableText\Observers\EditableTextObserver as ETObserver;

class ETServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/editabletext.php' => config_path('editabletext.php')
        ], 'public');

        $this->loadViewsFrom(__DIR__ . '/Views', 'et');

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        Blade::directive('et', function ($expression) {
            return sprintf("<?php echo \Sterrk\EditableText\Utilities\EditableText::renderText(%s); ?>", $expression);
        });

        Blade::directive('ei', function ($expression) {
            return sprintf("<?php echo \Sterrk\EditableText\Utilities\EditableText::renderImage(%s); ?>", $expression);
        });

        $this->publishes([
            __DIR__ . '/Views' => resource_path('views/vendor/et'),
        ], 'views');

        $this->publishes([
            __DIR__ . '/../resources' => resource_path('assets/vendor/et')
        ], 'resources');

        $this->publishes([
            __DIR__ . '/../public' => public_path('vendor/et')
        ], 'public');

        ETModel::observe(ETObserver::class);
    }

    public function register()
    {
        include __DIR__ . '/../routes/et.php';
        $this->app->make('Sterrk\EditableText\Controllers\EditableTextController');
    }
}
