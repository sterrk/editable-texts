<?php

namespace Sterrk\EditableText\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Sterrk\EditableText\Models;
use Sterrk\EditableText\Events\EditableTextPublished;
use Sterrk\EditableText\Utilities\EditableText;

class EditableTextController extends Controller
{
    const STORAGE_PATH = 'editable_images';
    const TMP_PATH = 'tmp';

    protected $fileName;

    /**
     * @param string $editmode
     * @return mixed
     */
    public function editMode($editmode)
    {
        $state = $editmode == "true";
        EditableText::setEditMode($state);
        return redirect()->to('/');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish(Request $request)
    {
        if ($request->input('changes') == null) {
            return response()->json([
                'status' => 1,
                'changes' => 0
            ]);
        }

        $changes = $request->get('changes');

        $ETcollection = Models\EditableText::all();
        foreach ($changes as $et) {
            $model = $ETcollection
                ->where('category', $et['category'])
                ->where('key', $et['key'])
                ->first();
            $model->value = $et['type'] === 'text' ? $et['value'] : $this->uploadImage($et);
            $model->save();
        }

        event(new EditableTextPublished(count($changes)));

        return response()->json([
            'status' => 1,
            'changes' => count($changes)
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        $value = EditableText::getText($request->input('category'), $request->input('key'), "Placeholder");

        return response()->json(compact('value'));
    }

    /**
     * @param array $etImage
     * @return mixed
     */
    protected function uploadImage($etImage)
    {
        $this->decodeBase64($etImage);

        $storagePath = self::STORAGE_PATH . '/' . $this->fileName;
        $tmpPath = storage_path(self::TMP_PATH) . '/' . $this->fileName;

        Storage::disk(config('editabletext.image_upload_location'))->put($storagePath, file_get_contents($tmpPath), 'public');
        unlink($tmpPath);
        return Storage::disk(config('editabletext.image_upload_location'))->url($storagePath);
    }

    /**
     * @param array $etImage
     * @return string
     */
    protected function decodeBase64($etImage)
    {
        $base64Data = explode(',', $etImage['value']);
        $imgData = base64_decode($base64Data[1]);

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);
        $ext = $this->mime2ext($mime_type);

        $this->fileName = $etImage['category'] . '_' . $etImage['key'] . '.' . $ext;

        if (!is_dir(storage_path(self::TMP_PATH))) {
            mkdir(storage_path(self::TMP_PATH), 0775);
        }

        file_put_contents(storage_path(self::TMP_PATH) . '/' . $this->fileName, $imgData);
    }

    /**
     * @param $mime
     * @return bool|string
     */
    protected function mime2ext($mime)
    {
        $mime_map = [
            'text/plain'                => 'svg',
            'image/bmp'                 => 'bmp',
            'image/x-bmp'               => 'bmp',
            'image/x-bitmap'            => 'bmp',
            'image/x-xbitmap'           => 'bmp',
            'image/x-win-bitmap'        => 'bmp',
            'image/x-windows-bmp'       => 'bmp',
            'image/ms-bmp'              => 'bmp',
            'image/x-ms-bmp'            => 'bmp',
            'application/bmp'           => 'bmp',
            'application/x-bmp'         => 'bmp',
            'application/x-win-bitmap'  => 'bmp',
            'image/gif'                 => 'gif',
            'image/x-icon'              => 'ico',
            'image/x-ico'               => 'ico',
            'image/vnd.microsoft.icon'  => 'ico',
            'image/jpeg'                => 'jpeg',
            'image/pjpeg'               => 'jpeg',
            'image/png'                 => 'png',
            'image/x-png'               => 'png',
        ];
        return isset($mime_map[$mime]) ? $mime_map[$mime] : false;
    }
}
