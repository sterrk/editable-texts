<?php

if (!function_exists('et')) {
    function et($category, $key, $defaultValue = null, $excludedFromEditMode = false)
    {
        return \Sterrk\EditableText\Utilities\EditableText::renderText($category, $key, $defaultValue, $excludedFromEditMode);
    }
}
