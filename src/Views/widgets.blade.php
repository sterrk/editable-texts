@if (\Sterrk\EditableText\Utilities\EditableText::isEditModeEnabled())
@include('et::flyout')
@include('et::text_modal')
@include('et::image_modal')
@include('et::loader')
@endif

