<div id="ETEditModal" class="yquery-modal et-edit-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="h2 modalTitle">Edit text</div>
    <div aria-hidden="true" class="et-category-label et-hidden"></div>
    <div aria-hidden="true" class="et-key-label et-hidden"></div>
    <div aria-hidden="true" class="et-text-db et-hidden"></div>
    <textarea class="et-text-area" rows="6"></textarea>
    <div class="et-button-container">
        <a class="button preview">Preview</a>
        <a class="button revert">Revert</a>
    </div>
    <a class="close-yquery-modal" data-modal="#ETEditModal" aria-label="Close">&#215;</a>
</div>
