<div id="EIEditModal" class="yquery-modal et-edit-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <div class="h2 modalTitle">Upload image</div>
    <input type="file" name="image">
    <div class="et-button-container">
        <a class="button revert">Revert</a>
    </div>
    <a class="close-yquery-modal" data-modal="#EIEditModal" aria-label="Close">&#215;</a>
</div>
