Editable Texts
==============

A Laravel package that allows you to edit text and images on your website, right there
on the page itself!  A replacement for the old back-end content management
systems;  Why edit your front-end in the back-end, when you can do it in place?

### Usage

The `sterrk/et` package offers two ways to include its functionality in your
page, through php helper functions and blade directives:


##### editable text:
- ```@et('category', 'key', 'default value')```

- ```{!! et('category', 'key', 'default value') !!}```

##### editable image:
- ```@ei('category', 'key', 'default value')```

- ```{!! ei('category', 'key', 'default value') !!}```

This sets up a system where the package pulls the text values from the database,
much like a traditional CMS would.

In addition, the package offers some pre-built solutions to manage your editable
texts in the front-end in the form of a flyout and a modal that can be included
by following the optional steps in the installation.

### Making local changes

This package publishes its views to `resources/views/vendor/et`.  These views
override the ones in the package without breaking updates, and can be changed
to your liking.  Similarly, all of the included assets (sass, js) are published
to `resources/assets/vendor/et` and can be changed without breaking updates.

Installation
============

1. `composer require sterrk/et`
2. `php artisan vendor::publish`
3. `php artisan migrate`

 *optional*

4. Define routes in your app using `Route::etRoutes($prefix)`, default prefix is
   'et'.
5. Add an entry point.  The easiest way is a button that has the following href
   in a blade template: `{{ route('et.editmode', ['editmode' => 'true']) }}`
6. Include the et widgets in your layout: `@include('et::widgets');`
7. Include sass and js files for the widgets into your build service:
    - `resources/assets/vendor/et/sass/package.scss`
    - `resources/assets/vendor/et/js/bundle.js`

   Or include them directly on your page from your public folder:
    - `public/vendor/et/css/package.min.css`
    - `public/vendor/et/js/bundle.min.js`
8. Images are by default uploaded to the project specific default disk configured in the `config/filesystems.php`.
   In order to make use of the `local` disk, please make sure to run `php artisan storage:link`. If you want to make use
   of a different disk for uploading files through ET you may configure this within `config/editabletext.php`.
