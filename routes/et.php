<?php

/*
|--------------------------------------------------------------------------
| ET Routes
|--------------------------------------------------------------------------
|
| Define routes for ET actions here.  This macro needs to be called in the
| parent route definitions to set up the ET routes:
|
| Route::etRoutes($prefix);
*/

Route::macro('etRoutes', function ($prefix = 'et') {
    Route::name('et.')->prefix($prefix)->group(function () {
        Route::get('edit-mode/{editmode}', '\Sterrk\EditableText\Controllers\EditableTextController@editMode')
            ->name('editmode');
        Route::post('publish', '\Sterrk\EditableText\Controllers\EditableTextController@publish')
            ->name('publish');
        Route::get('get', '\Sterrk\EditableText\Controllers\EditableTextController@get')
            ->name('get');
    });
});
