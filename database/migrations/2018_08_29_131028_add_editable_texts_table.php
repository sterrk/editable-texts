<?php
// phpcs:ignoreFile

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEditableTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('editable_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category');
            $table->string('key');
            $table->longText('value')->nullable();
            $table->string('set', 100)->nullable();
            $table->timestamps();
            $table->unique(['category', 'key'], 'category_key_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('editable_texts');
    }
}
